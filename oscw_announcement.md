﻿[IMAGE OSCW Logo with alt="Open Source Cubesat Workshop" for SEO]

November 23-24, 2017
Darmstadt, Germany

Scope
=====

Open source software (OSS) is used virtually everywhere and open source hardware (OSHW) is getting more and more popular as well. The Open Source Cubesat Workshop (OSCW) was created to foster the open source philosophy in cubesat missions and further. Its first edition takes place in the heart of the European Space Operations Center.

CubeSats have proven to be an ideal tool for disrupting old (space) traditions and inspire bigger missions: therefore let’s remove the barrier of confidentiality and secrecy, and start to freely share knowledge and information about how to build and operate CubeSats. This workshop provides a forum for CubeSat developers and CubeSat mission operators to meet and join forces on open source projects to benefit from transparency, inclusivity, adaptability, collaborations and community, the keys of the open source philosophy.
	
The workshop provides a forum for presenting CubeSat related open source projects and to exchange ideas and knowledge in CubeSat developments. The focus of this year’s workshop is on mission operations and how the open source approach applied to space and ground segment might impact this important area. The  target audience of this workshop is academia, research institutes, companies, and individuals.

Topics
======

The workshop invites presentations covering all aspects  of CubeSat missions involving open source assets. This includes, but does not limit to, the following categories:

 - Ongoing and planned missions
 - Instruments and payloads
 - Avionics subsystems
 - Ground stations and networks
 - Data and knowledge management
 - Mission operations
 - Artificial Intelligence in control and monitoring
 - Business models and regulations
 - Communities and organizations
 - Standards and reference architectures


Program
=======

The two day program is (preliminary) structured as follows:

Day 1
Morning	Keynotes and presentations
Afternoon	Presentations and round tables

Day 2
Morning	Presentations and pitch talks
Afternoon	Presentations and open discussions


Call for Abstracts
==================

If you like to give a 15 min presentation plus 5 min of Q&A, please register and submit an abstract (half to one page) to:
abstract@oscw.space


Registration
============

The workshop is limited to 80 people. To register, please fill out the form at:
oscw.space/registration

Registration fee is (participative? TBD) 40 Euro. This covers the costs of coffee breaks and the workshop dinner in a local German brewery restaurant.


Important Dates
===============

Submission of abstract:		21 September
Notification of acceptance:	9 October		
Registration closure:		9 November
Workshop:				23-24 November


Organizers
==========

ESOC Cybernetics Club
LibreSpace Foundation (http://libre.space)
LibreCube Initiative (http://librecube.net) 


Venue
=====

The event will be hosted at the European Space Operations Center in Darmstadt, Germany. This is the place from which the Rosetta mission was operated!

[IMAGE ESA/ESOC Main Control Room]

Address
=======

European Space Operations Center(ESOC/ESA)
Robert-Bosch-Str. 5, 64293 Darmstadt, Germany

[IMAGE ESA/ESOC Main Gate]

Contact
=======

For any questions or feedback, write to the OSCW’17 organization team at:

info@oscw.space

