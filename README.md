Open Source Cubesat Workshop
============================

Working repository to exchange information on the organisation of the OSCW.

You may find all what you need to organize your own Open Source Cubesat Workshop


Open Bookmark of links shared at #OSCW17
========================================


- [https://github.com/JuliaAstrodynamics/Astrodynamics.jl](https://github.com/JuliaAstrodynamics/Astrodynamics.jl)
- [https://github.com/poliastro/poliastro](https://github.com/poliastro/poliastro)
- [https://github.com/openastro](https://github.com/openastro)

... to be continued